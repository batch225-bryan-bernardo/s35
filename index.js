// Initialization, Connection
const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 5000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }))

mongoose.connect("mongodb+srv://bernardo_bryan2396:BpB072396!@cluster0.wizmpvw.mongodb.net/S35Activity?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("Connected to MongoDB."));


// User schema 

const registrySchema = mongoose.Schema({

    username: String,
    password: String

})

// User Model
const Registry = mongoose.model("Registry", registrySchema);

app.post("/signup", (req, res) => {

    Registry.findOne({ username: req.body.username }, (err, result) => {
        if (result != null && result.username == req.body.username) {
            return res.send("Duplicate user found");
        } else {
            let newRegistry = new Registry({
                username: req.body.username,
                password: req.body.password
            });
            newRegistry.save((saveErr, savedTask) => {
                if (saveErr) {
                    return console.error(saveErr);
                } else {
                    return res.status(201).send("New user registered");
                }
            })
        }
    }
    )
})
app.get("/signup", (req, res) => {
    Registry.find({}, (err, result) => {

        if (err) {
            return console.log(err);
        } else {
            return res.status(200).json({
                data: result
            })

        }

    })
})


// Port connect
app.listen(port, () => console.log(`Server running at localhost: ${port}`));